package com.example.samplelivelikesdktest.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.samplelivelikesdktest.databinding.WidgetViewBinding
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.LiveLikeWidget


class WidgetAdapter(private val context: Context, private val engagementSDK: EngagementSDK?,private val session: LiveLikeContentSession?) :
    RecyclerView.Adapter<WidgetAdapter.WidgetViewHolder>()  {

    lateinit var bindind: WidgetViewBinding

    inner class WidgetViewHolder(private val binding: WidgetViewBinding) : RecyclerView.ViewHolder(binding.root)


    init {
        setHasStableIds(true)
    }

    val list: ArrayList<LiveLikeWidget> = arrayListOf()
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): WidgetViewHolder {
        bindind = WidgetViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WidgetViewHolder(bindind)
    }


    override fun getItemViewType(position: Int): Int {
        return list[position].id?.hashCode() ?: position
    }

    override fun onBindViewHolder(viewHolder: WidgetViewHolder, position: Int) {
        val liveLikeWidget = list[position]

        /*bindind.widgetView.enableDefaultWidgetTransition = false
        if (session != null) {
            bindind.widgetView.setSession(session)
        }

        bindind.widgetView.widgetViewFactory = object : LiveLikeWidgetViewFactory {
            override fun createAlertWidgetView(alertWidgetModel: AlertWidgetModel): View? {
                return CustomAlert(context).apply {
                    this.alertModel = alertWidgetModel
                }

            }

            override fun createCheerMeterView(cheerMeterWidgetModel: CheerMeterWidgetmodel): View? {
               return CustomCheerMeter(context).apply {
                    this.cheerMeterWidgetmodel = cheerMeterWidgetModel
                    this.isTimeLine = true
                }

            }

            override fun createImageSliderWidgetView(imageSliderWidgetModel: ImageSliderWidgetModel): View? {
                return null
            }

            override fun createPollWidgetView(
                pollWidgetModel: PollWidgetModel,
                isImage: Boolean
            ): View? {
               return null
            }

            override fun createPredictionFollowupWidgetView(
                followUpWidgetViewModel: FollowUpWidgetViewModel,
                isImage: Boolean
            ): View? {
               return null
            }

            override fun createPredictionWidgetView(
                predictionViewModel: PredictionWidgetViewModel,
                isImage: Boolean
            ): View? {
                return null
            }

            override fun createQuizWidgetView(
                quizWidgetModel: QuizWidgetModel,
                isImage: Boolean
            ): View? {
                return null
            }

        }
*/
       if (engagementSDK != null) {
            bindind.widgetView.displayWidget(
                engagementSDK,
                liveLikeWidget
            )
        }
    }

    override fun getItemCount(): Int = list.size

}