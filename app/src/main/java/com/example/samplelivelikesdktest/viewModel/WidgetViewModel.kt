package com.example.samplelivelikesdktest.viewModel

import androidx.lifecycle.AndroidViewModel
import com.example.samplelivelikesdktest.LiveLikeSampleApp
import com.livelike.engagementsdk.LiveLikeContentSession

class WidgetViewModel constructor(
        application: LiveLikeSampleApp
) : AndroidViewModel(application) {

    val engagementSDK = application.getEngagementSdk()

    val contentSession = application.getContentSession()

    fun getSession(): LiveLikeContentSession? {
        return contentSession
    }

    fun pauseSession() {
        contentSession?.pause()
    }

    fun resumeSession() {
        contentSession?.resume()
    }

    fun closeSession() {
        contentSession?.close()
    }
}