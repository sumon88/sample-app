package com.example.samplelivelikesdktest.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.samplelivelikesdktest.LiveLikeSampleApp
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.LiveLikeContentSession

class ChatViewModel constructor(application: LiveLikeSampleApp) : AndroidViewModel(application) {


    val engagementSDK = application.getEngagementSdk()

    val contentSession = application.getContentSession()


    fun pauseSession() {
        contentSession?.pause()
    }

    fun resumeSession() {
        contentSession?.resume()
    }

    fun closeSession() {
        contentSession?.close()
    }

}