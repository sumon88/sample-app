package com.example.samplelivelikesdktest.ui

import android.app.Application
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.samplelivelikesdktest.databinding.PrivateChatLayoutBinding
import com.example.samplelivelikesdktest.viewModel.ChatViewModel
import com.example.samplelivelikesdktest.viewModel.ChatViewModelFactory
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.LiveLikeUser
import com.livelike.engagementsdk.chat.ChatRoomInfo
import com.livelike.engagementsdk.chat.Visibility
import com.livelike.engagementsdk.chat.data.remote.ChatRoomMembership
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeCallback

class PrivateChatActivity:AppCompatActivity() {

    private lateinit var binding: PrivateChatLayoutBinding
    var chatViewModel: ChatViewModel? = null
    private var chatRoomIds: MutableSet<String> = mutableSetOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = PrivateChatLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initViewModel()
        initListeners()
    }


    private fun initViewModel(){
        chatViewModel = ViewModelProvider(
            this,
            ChatViewModelFactory(this.applicationContext as Application)
        ).get(ChatViewModel::class.java)
    }


    private fun initListeners(){
        binding.btnCreate.setOnClickListener {
            val title = binding.chatroomText.text.toString()
            binding.progressBar.visibility = View.VISIBLE
            chatViewModel?.engagementSDK?.createChatRoom(
                title,
                null,
                object : LiveLikeCallback<ChatRoomInfo>() {
                    override fun onResponse(result: ChatRoomInfo?, error: String?) {
                       /* binding.textView2.text = when {
                            //result != null -> "${result.title ?: "No Title"}(${result.id})"
                           // result != null -> "${result.title ?: "No Title"}(created)"
                            result != null -> Toast.makeText(this@PrivateChatActivity,"${result.title}created",Toast.LENGTH_LONG).show()
                                .toString()
                            else -> error
                        }*/
                        result?.let {
                            Toast.makeText(this@PrivateChatActivity,"${result.title}created",Toast.LENGTH_LONG).show()
                                    .toString()
                            chatRoomIds.add(it.id)
                        }
                        binding.progressBar.visibility = View.GONE
                    }
                })
        }


        binding.btnJoin.setOnClickListener {
            val chatRoomId = binding.joinroomText.text.toString()
            if (chatRoomId.isEmpty().not()) {
                chatRoomIds.add(chatRoomId)
               // binding.joinroomText.setText("")

                chatViewModel?.engagementSDK?.addCurrentUserToChatRoom(chatRoomId,
                        object : LiveLikeCallback<ChatRoomMembership>() {
                            override fun onResponse(result: ChatRoomMembership?, error: String?) {
                                result?.let {
                                    showToast("User Added Successfully")
                                }
                                error?.let {
                                    showToast(it)
                                }
                            }
                        })
            }
        }

        binding.btnUnjoin.setOnClickListener {
            val chatRoomId = binding.joinroomText.text.toString()
            if (chatRoomId.isEmpty().not()) {
                chatRoomIds.remove(chatRoomId)
                chatViewModel?.engagementSDK?.deleteCurrentUserFromChatRoom(chatRoomId,
                        object : LiveLikeCallback<Boolean>() {
                            override fun onResponse(result: Boolean?, error: String?) {
                                result?.let {
                                    showToast("Deleted ChatRoom")
                                }
                            }
                        })
            }
        }


        binding.viewMembers.setOnClickListener { viewMembersOfChatRoom() }
    }


    private fun viewMembersOfChatRoom() {
        val chatRoomId = binding.joinroomText.text.toString()
        if (chatRoomId.isEmpty().not()){
            chatViewModel?.engagementSDK?.getMembersOfChatRoom(chatRoomId,
                    LiveLikePagination.FIRST,
                    object : LiveLikeCallback<List<LiveLikeUser>>() {
                        override fun onResponse(result: List<LiveLikeUser>?, error: String?) {
                            result?.let { list ->
                                if (list.isNotEmpty()) {
                                    AlertDialog.Builder(this@PrivateChatActivity).apply {
                                        setTitle("Room Members")
                                        setItems(list.map { it.nickname }
                                                .toTypedArray()) { _, which ->
                                            // On change of theme we need to create the session in order to pass new attribute of theme to widgets and chat
                                        }
                                        create()
                                    }.show()
                                }
                            }
                        }
                    })
    }
    }


    private fun enterPrivateChat(){
        /*val chatSession = engagementSDK?.createContentSession(PROGRAM_ID)
        chatSession?.chatSession?.enterChatRoom("40b95259-2ee1-44ee-a548-b261f7fbf079")
        binding.chatView.setSession(chatSession.chatSession)*/
    }


    private fun buildRoomsforPrivateChat(){
        chatViewModel?.engagementSDK?.createChatRoom(
                "Test",
                Visibility.everyone,
                object : LiveLikeCallback<ChatRoomInfo>() {
                    override fun onResponse(result: ChatRoomInfo?, error: String?) {
                        val response = when {
                            result != null -> "${result.title ?: "No Title"}(${result.id})"
                            else -> error
                        }
                        response?.let { it1 -> showToast(it1) }
                    }
                })
    }


    private fun updateChatRoom(title:String,vis:Visibility){
        chatViewModel?.engagementSDK?.updateChatRoom("40b95259-2ee1-44ee-a548-b261f7fbf079",
                title,
                vis,
                object : LiveLikeCallback<ChatRoomInfo>() {
                    override fun onResponse(result: ChatRoomInfo?, error: String?) {

                        error?.let {
                            showToast(it)
                        }
                    }
                })
    }

    private fun showToast(title:String){
        Toast.makeText(this,title,Toast.LENGTH_SHORT).show()
    }



}