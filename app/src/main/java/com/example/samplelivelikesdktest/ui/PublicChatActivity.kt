package com.example.samplelivelikesdktest.ui

import android.app.Application
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.samplelivelikesdktest.*
import com.example.samplelivelikesdktest.databinding.EngagementActivityBinding
import com.example.samplelivelikesdktest.viewModel.ChatViewModel
import com.example.samplelivelikesdktest.viewModel.ChatViewModelFactory

/**
 * Use this class to display public chat details.
 *
 */
class PublicChatActivity:AppCompatActivity() {

    private lateinit var binding: EngagementActivityBinding
    var chatViewModel: ChatViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setThemeForChat()
        binding = EngagementActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initViewModel()
        getValuesFromIntent()
    }



    private fun initViewModel(){
        (application as LiveLikeSampleApp).getClientProgramId()?.let { (application as LiveLikeSampleApp).createSession(it,null) }
        chatViewModel = ViewModelProvider(
                this,
                ChatViewModelFactory(this.applicationContext as Application)
        ).get(ChatViewModel::class.java)
    }


    private fun getValuesFromIntent(){
        if(intent!=null) {
            var type = intent.getStringExtra(PUBLIC_CHAT_KEY)

            when (type){
                 "public" -> buildPublicChat()
                 "private" -> navigateToPrivateChat()
            }
        }
    }

    /**
     * set chat session
     * which will basically be responsible for displaying the chat view
     */
    private fun buildPublicChat(){
        chatViewModel?.contentSession?.let {
            binding.chatView.setSession(it.chatSession)
            chatViewModel?.engagementSDK?.updateChatNickname("Sumon")
        }
    }


    private fun navigateToPrivateChat(){
        var intent = Intent(this, PrivateChatActivity::class.java)
        startActivity(intent)
    }

    /**
     * choose theme popup dialog
     *
     */
    private fun setThemeForChat(){
       var themeName = (application as LiveLikeSampleApp).getChatThemeName()
        when {
            themeName.equals(DEFAULT_CHAT_THEME) -> {
                setTheme(R.style.DefaultChatTheme)

            }
            themeName.equals(CUSTOM_CHAT_THEME) -> {
                setTheme(R.style.CustomChatTheme)
            }

            themeName.equals(STYLISH_CHAT_THEME) -> {
                setTheme(R.style.StylishChatTheme)
            }
            else -> {
                setTheme(R.style.DefaultChatTheme)
            }
        }
    }




    override fun onResume() {
        super.onResume()
        chatViewModel?.contentSession?.resume()
    }

    override fun onPause() {
        super.onPause()
        chatViewModel?.contentSession?.pause()
    }

    override fun onDestroy() {
        super.onDestroy()
        chatViewModel?.contentSession?.close()
    }
}