package com.example.samplelivelikesdktest.ui

import android.app.Application
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.samplelivelikesdktest.Utils
import com.example.samplelivelikesdktest.databinding.SingleWidgetBinding
import com.example.samplelivelikesdktest.viewModel.WidgetViewModel
import com.example.samplelivelikesdktest.viewModel.WidgetViewModelFactory
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.WidgetListener
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.engagementsdk.widget.widgetModel.*

class SingleWidgetActivity:AppCompatActivity() {

    private lateinit var binding: SingleWidgetBinding
    var widgetViewModel: WidgetViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SingleWidgetBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initViewModel()
        getPublishedWidget()

    }


    private fun initViewModel(){
        widgetViewModel = ViewModelProvider(
             this,
            WidgetViewModelFactory(this.applicationContext as Application)
        ).get(WidgetViewModel::class.java)
    }


    private fun getPublishedWidget(){
        // instant widget publish
        binding.widgetView.setWidgetListener(object : WidgetListener {
            override fun onNewWidget(liveLikeWidget: LiveLikeWidget) {
                println("Widget:${liveLikeWidget.id},${liveLikeWidget.programId},${liveLikeWidget.options?.size}")
            }
        })

        widgetViewModel?.contentSession?.let { binding.widgetView.setSession(it) }
        binding.progressBar.visibility = View.VISIBLE
        widgetViewModel?.contentSession?.getPublishedWidgets(LiveLikePagination.FIRST,
            object : LiveLikeCallback<List<LiveLikeWidget>>() {
                override fun onResponse(result: List<LiveLikeWidget>?, error: String?) {
                    result?.map { it!! }.let {
                        widgetViewModel?.engagementSDK?.let { it1 ->
                            Utils.showMyWidgetsDialog(this@SingleWidgetActivity,
                                it1,
                                ArrayList(it),
                                object : LiveLikeCallback<LiveLikeWidget>() {
                                    override fun onResponse(
                                        result: LiveLikeWidget?,
                                        error: String?
                                    ) {
                                        result?.let {
                                            widgetViewModel?.engagementSDK?.let { it1 ->
                                                binding.widgetView.displayWidget(
                                                    it1,
                                                    result
                                                )
                                                binding.progressBar.visibility = View.GONE
                                            }
                                        }
                                    }
                                })
                        }
                    }
                }
            })
    }
}