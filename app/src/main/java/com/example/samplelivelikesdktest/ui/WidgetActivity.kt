package com.example.samplelivelikesdktest.ui

import android.app.AlertDialog
import android.app.Application
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.samplelivelikesdktest.adapter.WidgetAdapter
import com.example.samplelivelikesdktest.databinding.WidgetAcivityBinding
import com.example.samplelivelikesdktest.viewModel.WidgetViewModel
import com.example.samplelivelikesdktest.viewModel.WidgetViewModelFactory
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.chat.data.remote.LiveLikePagination
import com.livelike.engagementsdk.core.services.messaging.proxies.LiveLikeWidgetEntity
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetInterceptor
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import com.livelike.engagementsdk.widget.widgetModel.*

/**
 * Class responsible for showing all the published widget
 *
 */
class WidgetActivity: AppCompatActivity() {

    private lateinit var binding: WidgetAcivityBinding
    var widgetViewModel: WidgetViewModel? = null
    var adapter: WidgetAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = WidgetAcivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initViewModel()
        initAdapter()
        initListeners()
    }


    private fun initViewModel(){
        widgetViewModel = ViewModelProvider(
                this,
                WidgetViewModelFactory(this.applicationContext as Application)
        ).get(WidgetViewModel::class.java)
    }


    private fun initAdapter() {
        // Example of Widget Interceptor showing a dialog
        val interceptor = object : WidgetInterceptor() {
            override fun widgetWantsToShow(widgetData: LiveLikeWidgetEntity) {
                AlertDialog.Builder(this@WidgetActivity).apply {
                    setMessage("You received a Widget, what do you want to do?")
                    setPositiveButton("Show") { _, _ ->
                        showWidget() // Releases the widget
                    }
                    setNegativeButton("Dismiss") { _, _ ->
                        dismissWidget() // Discards the widget
                    }
                    create()
                }.show()
            }
        }

       // widgetViewModel?.contentSession?.let { binding.widgetViewContainer.setSession(it) }

        widgetViewModel?.contentSession?.widgetInterceptor = interceptor
        adapter = WidgetAdapter(this, widgetViewModel?.engagementSDK,widgetViewModel?.contentSession)
        binding.rcylWidgets.adapter = adapter
      /*  binding.swipeContainer.setOnRefreshListener {
            loadData(LiveLikePagination.FIRST)
        }*/

        loadData(LiveLikePagination.FIRST)
    }


    private fun initListeners(){
       /* binding.rcylWidgets.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            private var loading: Boolean = true
            var pastVisiblesItems = 0
            var visibleItemCount: Int = 0
            var totalItemCount: Int = 0
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) { //check for scroll down
                    visibleItemCount =  binding.rcylWidgets.layoutManager!!.childCount
                    totalItemCount =  binding.rcylWidgets.layoutManager!!.itemCount
                    pastVisiblesItems =
                        (binding.rcylWidgets.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false
                            loadData(LiveLikePagination.NEXT)
                            loading = true
                        }
                    }
                }
            }
        })*/
    }




    private fun loadData(liveLikePagination: LiveLikePagination) {
        binding.progressBar.visibility = View.VISIBLE
        widgetViewModel?.contentSession?.getPublishedWidgets(
                liveLikePagination,
                object : LiveLikeCallback<List<LiveLikeWidget>>() {
                    override fun onResponse(result: List<LiveLikeWidget>?, error: String?) {
                        result?.let { list ->
                            if (liveLikePagination == LiveLikePagination.FIRST) {
                                adapter?.list?.clear()
                            }
                            adapter?.list?.addAll(list.map { it!! })
                            adapter?.notifyDataSetChanged()
                            binding.progressBar.visibility = View.GONE
                            binding.rcylWidgets.visibility = View.VISIBLE
                            binding.swipeContainer.isRefreshing = false
                        }
                    }
                })
    }
}