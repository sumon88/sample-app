package com.example.samplelivelikesdktest.ui


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.samplelivelikesdktest.*
import com.example.samplelivelikesdktest.databinding.ActivityMainBinding
import com.livelike.engagementsdk.EngagementSDK


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    var alertDialog:AlertDialog? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setListeners()
    }


    private fun setListeners(){
        binding.chat.setOnClickListener { showCustomDialog() }
        binding.widget.setOnClickListener { navigateToWidget() }
        binding.customWidget.setOnClickListener { navigateToCustomWidgets() }
    }

    private fun navigateToPublicChat(type: String){
        alertDialog?.dismiss()
        var intent = Intent(this, PublicChatActivity::class.java)
        intent.putExtra(PUBLIC_CHAT_KEY, type)
        startActivity(intent)
    }

    private fun navigateToWidget(){
        alertDialog?.dismiss()
        var intent = Intent(this, SingleWidgetActivity::class.java)
        startActivity(intent)
    }


    private fun navigateToCustomWidgets(){
        alertDialog?.dismiss()
        var intent = Intent(this, WidgetActivity::class.java)
        startActivity(intent)
    }


    private fun showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity view group
        val viewGroup = findViewById<ViewGroup>(android.R.id.content)

        //then we will inflate the custom alert dialog xml that we created
        val dialogView: View = LayoutInflater.from(this).inflate(R.layout.chat_dialog, viewGroup, false)

        val privateChats: Button = dialogView.findViewById(R.id.privateChat)
        val publicChats: Button = dialogView.findViewById(R.id.publicChat)
        val chatTheme: Button = dialogView.findViewById(R.id.chatTheme)

        //Now we need an AlertDialog.Builder object
        val builder = AlertDialog.Builder(this)

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it
        alertDialog = builder.create()

        privateChats.setOnClickListener { navigateToPublicChat("private") }

        publicChats.setOnClickListener { navigateToPublicChat("public") }

        chatTheme.setOnClickListener { openCustomChatTheme() }

        alertDialog?.show()
    }


    private fun openCustomChatTheme(){
            val channels = arrayListOf("Default", "Custom Chat Theme", "Stylish Chat theme")
            android.app.AlertDialog.Builder(this).apply {
                setTitle("Choose a theme!")
                setItems(channels.toTypedArray()) { _, which ->
                    // On change of theme we need to create the session in order to pass new attribute of theme to widgets and chat
                    (application as LiveLikeSampleApp).removeSession()
                    EngagementSDK.enableDebug = false
                    when (which) {
                        0 -> {
                            (application as LiveLikeSampleApp).setChatThemes(DEFAULT_CHAT_THEME)
                            }
                        1 -> {
                            EngagementSDK.enableDebug = false
                            (application as LiveLikeSampleApp).setChatThemes(CUSTOM_CHAT_THEME)
                        }

                        2 -> {
                            EngagementSDK.enableDebug = false
                            (application as LiveLikeSampleApp).setChatThemes(STYLISH_CHAT_THEME)
                        }

                        else -> {
                            (application as LiveLikeSampleApp).setChatThemes(DEFAULT_CHAT_THEME)
                        }
                    }
                }
                create()
            }.show()

    }

}