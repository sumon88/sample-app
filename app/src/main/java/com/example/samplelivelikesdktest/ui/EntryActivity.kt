package com.example.samplelivelikesdktest.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.samplelivelikesdktest.LiveLikeSampleApp
import com.example.samplelivelikesdktest.databinding.EntryLayoutBinding

class EntryActivity:AppCompatActivity() {

    private lateinit var binding: EntryLayoutBinding
    private var APP_CLIENT_ID_PROD = "mOBYul18quffrBDuq2IACKtVuLbUzXIPye5S3bq5"
    private var PROGRAM_ID = "8f34411b-159c-4840-b8ca-794785dfc1ca"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = EntryLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setListeners()
    }


    private fun setListeners(){
        binding.clientId.setText(APP_CLIENT_ID_PROD)
        binding.programId.setText(PROGRAM_ID)

        binding.continueBtn.setOnClickListener {
            if (!binding.clientId.text.isNullOrEmpty() && !binding.programId.text.isNullOrEmpty()) {
                (application as LiveLikeSampleApp).setclientId(binding.clientId.text.toString())
                (application as LiveLikeSampleApp).setProgramIdForClient(binding.programId.text.toString())
                navigateToMain()

            } else {
                Toast.makeText(this, "Client id cannot be blank", Toast.LENGTH_LONG).show()
            }
        }
    }



    private fun navigateToMain(){
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

}