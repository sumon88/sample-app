package com.example.samplelivelikesdktest.ui

import android.app.Application
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.samplelivelikesdktest.databinding.ChatThemeBinding
import com.example.samplelivelikesdktest.viewModel.ChatViewModel
import com.example.samplelivelikesdktest.viewModel.ChatViewModelFactory

class ChatThemingActivity:AppCompatActivity() {

    private lateinit var binding: ChatThemeBinding
    var chatViewModel: ChatViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ChatThemeBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initViewModel()

    }

    private fun initViewModel(){
        chatViewModel = ViewModelProvider(
            this,
            ChatViewModelFactory(this.applicationContext as Application)
        ).get(ChatViewModel::class.java)
    }

}