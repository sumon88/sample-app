package com.example.samplelivelikesdktest

import android.content.Context
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.LiveLikeWidget
import com.livelike.engagementsdk.publicapis.LiveLikeCallback
import java.time.format.DateTimeParseException

object Utils {

    fun showMyWidgetsDialog(
        context: Context,
        sdk: EngagementSDK,
        myWidgetsList: ArrayList<LiveLikeWidget>,
        liveLikeCallback: LiveLikeCallback<LiveLikeWidget>
    ) {
        AlertDialog.Builder(context).apply {
            setTitle("Published Widgets!")
            //setItems(myWidgetsList.map { "${it.id}(${it.kind})\nPublished:${it.publishedAt}\nCreated:${it.createdAt}" }
            setItems(myWidgetsList.map { "${it.id}(${it.kind})" }
                .toTypedArray()) { _, which ->
                val widget = myWidgetsList[which]
                sdk.fetchWidgetDetails(
                    widget.id!!,
                    widget.kind!!, liveLikeCallback
                )
            }


        }.create()
            .apply {
                show()
            }
    }



}