package com.example.samplelivelikesdktest

const val PUBLIC_CHAT_KEY = "CHAT_KEY"
const val WIDGET_KEY = "WIDGET_KEY"

const val DEFAULT_CHAT_THEME = "DEFAULT_CHAT_THEME"
const val CUSTOM_CHAT_THEME ="CUSTOM_CHAT_THEME"
const val STYLISH_CHAT_THEME ="STYLISH_CHAT_THEME"