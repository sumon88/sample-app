package com.example.samplelivelikesdktest

import android.app.Application
import com.livelike.engagementsdk.EngagementSDK
import com.livelike.engagementsdk.LiveLikeContentSession
import com.livelike.engagementsdk.core.services.messaging.proxies.WidgetInterceptor

class LiveLikeSampleApp: Application() {

    private var contentSession: LiveLikeContentSession? = null
    var engagementSDK: EngagementSDK? = null
    var programId: String? = null
    var clientId: String? = null
    var style:String? = null

    private var APP_CLIENT_ID_PROD = "mOBYul18quffrBDuq2IACKtVuLbUzXIPye5S3bq5"
    private var PROGRAM_ID = "8f34411b-159c-4840-b8ca-794785dfc1ca"

    override fun onCreate() {
        super.onCreate()

       // setclientId(APP_CLIENT_ID_PROD)
       // setProgramIdForClient(PROGRAM_ID)
    }

    fun setProgramIdForClient(id: String) {
        programId = id
        setContentSession()
    }

    fun getClientProgramId():String?{
        return programId
    }


    fun setclientId(id: String) {
        clientId = id
        setUpEngagementSDK()
    }

    private fun setUpEngagementSDK() {
        engagementSDK =
                clientId?.let { EngagementSDK(it, this) }
    }

    fun setContentSession() {
        contentSession =
                programId?.let { engagementSDK?.createContentSession(it) }
    }

    fun getContentSession(): LiveLikeContentSession? {
        return contentSession
    }

    fun getEngagementSdk(): EngagementSDK? {
        return engagementSDK
    }

    fun removeSession() {
        contentSession?.close()
        contentSession = null
    }

    /**
     * create a session. A session is kinda mandatory. inside a session
     * resides chat and widget
     *
     */
    fun createSession(
            sessionId: String,
            widgetInterceptor: WidgetInterceptor? = null
    ): LiveLikeContentSession {
        if (contentSession == null || contentSession?.contentSessionId() != sessionId) {
            contentSession?.close()
            contentSession = engagementSDK?.createContentSession(sessionId)
        }
       // contentSession!!.widgetInterceptor = widgetInterceptor
        return contentSession as LiveLikeContentSession
    }


  fun setChatThemes(theme:String){
      this.style= theme
  }

    fun getChatThemeName():String?{
        return style
    }

}